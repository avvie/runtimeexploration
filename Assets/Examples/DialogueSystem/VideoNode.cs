﻿using Dialogue;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using XNode;

namespace DialogueGraph {
	[System.Serializable]
	public class VideoNode : DialogueBaseNode {

		[TextArea] public string title;
		[TextArea]public string SentenceID;
		[SerializeField]public VideoClip videoClip;
		[Output(instancePortList = true)] public List<Answer> answers = new List<Answer>();

		[System.Serializable]
		public class Answer {
			public string text;
			public int score;
		}

		public VideoNode() {
			base.child = this;			
		}

		public void AnswerQuestion(int index) {
			NodePort port = null;
			if(answers.Count == 0) {
				port = GetOutputPort("output");
			}
			else {
				if(answers.Count <= index) return;
				port = GetOutputPort("answers " + index);
			}

			if(port == null) return;
			for(int i = 0; i < port.ConnectionCount; i++) {
				NodePort connection = port.GetConnection(i);
				(connection.node as DialogueBaseNode).Trigger();
			}
		}

		public VideoClip GetNextVideos(int i) {
			NodePort port = null;
			if(answers.Count != 0) {
				port = GetOutputPort("answers " + i);
				return (port.GetConnection(0).node as DialogueBaseNode).child.videoClip;
				//return ((port.GetConnection(0).node as DialogueBaseNode).graph as VideoGraphNode)
			}
			return null;
		}

		public override void Trigger() {
			Debug.Log("here");
			(graph as VideoDialogueGraph).current = this;
		}
	}
}