﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;
using System.Linq;
using XNode.InportExport;

namespace DialogueGraph {
	[CreateAssetMenu(menuName = "Dialogue/Video Graph", order = 1), System.Serializable]
	public class VideoDialogueGraph : NodeGraph {
		[HideInInspector]
		public VideoNode current;

		public void Restart() {
			current = nodes.Find(x => x is VideoNode && x.Inputs.All(y => !y.IsConnected)) as VideoNode;
		}

		public VideoNode AnswerQuestion(int i) {
			current.AnswerQuestion(i);
			return current;
		}

		[ContextMenu("Export")]
		public void Export() {
			JSONImportExport jep = new JSONImportExport();
			jep.Export(this, Application.dataPath + "/" + name + ".json");
		}

		[ContextMenu("Import")]
		public void Import() {
			string path = Application.dataPath + "/" + name + ".json";
			JSONImportExport jimp = new JSONImportExport();
			VideoDialogueGraph g = (VideoDialogueGraph)jimp.Import(path);

			if(g == null) return;

			Clear();

			nodes = g.nodes;
			foreach(Node n in nodes) {
				n.graph = this;
			}
		}
	}

}