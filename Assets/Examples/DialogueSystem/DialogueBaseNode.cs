﻿using DialogueGraph;
using XNode;

namespace Dialogue {
	[System.Serializable]
	public abstract class DialogueBaseNode : Node {
		[Input(backingValue = ShowBackingValue.Never)] public DialogueBaseNode input;
		[Output(backingValue = ShowBackingValue.Never)] public DialogueBaseNode output;

		public VideoNode child;

		abstract public void Trigger();

		public override object GetValue(NodePort port) {
			return null;
		}
	}
}